//
//  EquationResult.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 1/9/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import Foundation

enum EquationResult {
    case success(Double)
    case failure(EquationError)
}
