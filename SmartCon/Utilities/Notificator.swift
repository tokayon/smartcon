//
//  Notificator.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 5/8/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

class Notificator {
    
    class func post(_ name: NSNotification.Name, userInfo: [AnyHashable: Any]? = nil) {
        NotificationCenter.default.post(name: name, object: nil, userInfo: userInfo)
    }
    
    class func add(_ observer: Any, sel: Selector, name: NSNotification.Name?) {
        NotificationCenter.default.addObserver(observer, selector: sel, name: name, object: nil)
    }
    
    class func remove(_ observer: Any, name: NSNotification.Name?) {
        NotificationCenter.default.removeObserver(observer, name: name, object: nil)
    }
    
}
