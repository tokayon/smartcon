//
//  Presenter.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 1/11/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit

class Presenter: NSObject {
    class func checkedTitle(for convert: Convert, at row: Int) -> CheckedTitle {
        var checkedTitle = CheckedTitle()
        var empty = true
        var validTitle = false
        switch row {
        case 0:
            empty = convert.name.isEmpty
            let title = empty ? DetailsType.name.title : convert.name
            checkedTitle.title = title.attributed
            let valid = ConvertValidator.nameIsValid(for: convert)
            let existed = DataManager.shared.checkDublicate(of: convert.name)
            switch existed {
            case .success(let existed):
                validTitle = valid && !existed
            default:
                break
            }
        case 1:
            empty = convert.a.isEmpty
            let title = empty ? DetailsType.a.title : convert.a + " (a)"
            checkedTitle.title = title.attributed
            validTitle = ConvertValidator.aIsValid(for: convert)
        case 2:
            empty = convert.b.isEmpty
            let title = empty ? DetailsType.b.title : convert.b + " (b)"
            checkedTitle.title = title.attributedWhiteEquation
            validTitle = ConvertValidator.bIsValid(for: convert)
        case 3:
            if let equation = convert.equation {
                var title = equation.raw.contains("a") ? "b = " : "a = "
                title += equation.raw
                checkedTitle.title = title.attributedWhiteEquation
                empty = false
            } else {
                let title = DetailsType.equation.title
                checkedTitle.title = title.attributed
            }
            validTitle = ConvertValidator.equationIsValid(for: convert)
        default:
            break
        }
        checkedTitle.pointColor = empty ? UIColor.pointDark :
            validTitle ? UIColor.pointYellow : UIColor.pointRed
        return checkedTitle
    }
    
    class func append(value: String, to input: NSAttributedString?) -> NSAttributedString {
        let prevText = input ?? NSAttributedString()
        let output = NSMutableAttributedString(attributedString: prevText)
        output.append(value.attributedBlueEquation)
        return output
    }
    
    class func reverse(equation: NSAttributedString?, reverse: Bool) -> NSAttributedString? {
        guard let equation = equation else { return nil }
        let input = equation.string
        let replacing = reverse ? "b" : "a"
        let replaced = reverse ? "a" : "b"
        let output = input.replacingOccurrences(of: replacing, with: replaced)
        return output.attributedBlueEquation
    }
}
