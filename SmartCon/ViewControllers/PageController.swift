//
//  PageController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 5/4/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit
import CoreData

class PageController: UIPageViewController {
        
    var pageControl: UIPageControl?
    
    private(set) lazy var orderedViewControllers: [UIViewController] = []
    
    let dataManager = DataManager.shared
    
    private func createConvertController(convert: Convert) -> ConvertController {
        let vc: ConvertController = instantiateViewController()
        vc.convert = convert
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
        subscribeNotifications()
    }
    
    deinit {
        unsubscribeNotifications()
    }
}

// MARK: - Notifications
extension PageController {
    
    func subscribeNotifications() {
        Notificator.add(self, sel: #selector(updateConverts), name: .update)
        Notificator.add(self, sel: #selector(insertConvert), name: .insert)
    }
    
    func unsubscribeNotifications() {
        Notificator.remove(self, name: .update)
        Notificator.remove(self, name: .insert)
    }
}

// MARK: - Setup
private extension PageController {

    func initialSettings() {
        self.dataSource = self
        self.delegate = self
        self.view.backgroundColor = UIColor.white
        fetchConverts()
        dataManager.seed()
    }

    func fetchConverts() {
        let result = dataManager.fetchConverts()
        switch result {
        case .success(let convertEntities):
            guard let controllers = orderedViewControllers as? [ConvertController]
                else { return }
            for convertEntity in convertEntities {
                guard controllers.filter({ $0.convert.name == convertEntity.name }).isEmpty else { continue }
                guard let convert = dataManager.createConvert(from: convertEntity)
                    else { return }
                let convertController = createConvertController(convert: convert)
                orderedViewControllers.append(convertController)
                setupPageController()
            }
        case .failure(let error):
            print("Handle error here", error.localizedDescription)
        }
    }
    
    func fetchAndUpdateConverts() {
        let result = dataManager.fetchConverts()
        switch result {
        case .success(let convertEntities):
            orderedViewControllers.removeAll()
            for convertEntity in convertEntities {
                guard let convert = dataManager.createConvert(from: convertEntity)
                else { return }
                let convertController = createConvertController(convert: convert)
                orderedViewControllers.append(convertController)
            }
            setupPageController()
        case .failure(let error):
            print("Handle error here", error.localizedDescription)
        }
    }
    
    func setupPageController() {
        DispatchQueue.main.async {
            if let firstViewController = self.orderedViewControllers.first,
                let convertVC = firstViewController as? ConvertController {
                self.setViewControllers([firstViewController],
                                        direction: .forward,
                                        animated: true,
                                        completion: nil)
                self.title = convertVC.convert?.name
            }
            
            guard let pageControl = self.pageControl else {
                self.configurePageControl()
                return
            }
            pageControl.numberOfPages = self.orderedViewControllers.count
            pageControl.currentPage = 0
        }
    }
    
    func configurePageControl() {
        let y = UIScreen.main.bounds.maxY - 50
        let rect = CGRect(x: 0,y: y, width: UIScreen.main.bounds.width, height: 50)
        let pageControl = UIPageControl(frame: rect)
        pageControl.numberOfPages = orderedViewControllers.count
        pageControl.currentPage = 0
        let tint = UIColor.grayBlue
        let current = UIColor.darkGrayBlue
        pageControl.pageIndicatorTintColor = tint
        pageControl.currentPageIndicatorTintColor = current
        pageControl.backgroundColor = UIColor.white
        self.view.addSubview(pageControl)
        self.pageControl = pageControl
    }
}

// MARK: - Helpers
extension PageController {
    @objc func showDatabaseError() {
        
    }
    
    @objc func updateConverts(_ notification: Notification) {
        fetchAndUpdateConverts()
    }
    
    @objc func insertConvert(_ notification: Notification) {
        fetchConverts()
    }
}

// MARK: - Data Source
extension PageController: UIPageViewControllerDataSource {
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return orderedViewControllers.count
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
}

extension PageController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let controllers = pageViewController.viewControllers else { return  }
        guard let controller = controllers.first as? ConvertController else { return }
        self.pageControl?.currentPage = controller.convert?.orderIndex ?? 0
        self.title = controller.convert?.name ?? ""
    }
}
