//
//  Operator.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 11/30/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

enum Operator: Int, Codable {
    case unknown = 0
    case plus = 1
    case minus = 2
    case multiply = 3
    case divide = 4
    case inverse = 5
    
    var reversed: Operator {
        switch self {
        case .plus:
            return .minus
        case .minus:
            return .plus
        case .multiply:
            return .divide
        case .divide:
            return .multiply
        case .unknown:
            return .unknown
        case .inverse:
            return .inverse
        }
    }
    
    static func convert(value: Int) -> Operator {
        switch value {
        case 1:
            return .plus
        case 2:
            return .minus
        case 3:
            return .multiply
        case 4:
            return .divide
        case 5:
            return .inverse
        default:
            return .unknown
        }
    }

    static func convert(value: String) -> Operator {
        switch value {
        case "+":
            return .plus
        case "-":
            return .minus
        case "x","*":
            return .multiply
        case ":","/":
            return .divide
        default:
            return .unknown
        }
    }
    
    static func convert(value: Character) -> Operator {
        convert(value: String(value))
    }
    
}
