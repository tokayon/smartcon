//
//  BoolResult.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 3/3/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import Foundation

enum BoolResult {
    case success(Bool)
    case failure(Errors)
}
