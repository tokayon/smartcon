//
//  BuildResult.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 1/13/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import Foundation

struct BuildResult {
    var equation: Equation
    var error: EquationError?
    
    init(equation: Equation, error: EquationError? = nil) {
        self.equation = equation
        self.error = error
    }
}
