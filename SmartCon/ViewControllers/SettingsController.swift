//
//  SettingsController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 7/29/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit

class SettingsController: BaseTableController {
    @IBOutlet weak var tableView: UITableView!
    
    var titles = [Constants.Labels.customizeConverters,
                  Constants.Labels.legal,
                  Constants.Labels.about,
                  Constants.Labels.restorePurchases,
                  Constants.Labels.unlockApp]
    
    override func viewDidLoad() {
        self.title = Constants.Labels.settings
        checkIfUnlocked()
        Notificator.add(self, sel: #selector(restoredUpdate), name: .restored)
        Notificator.add(self, sel: #selector(purchasedUpdate), name: .purchased)
        Notificator.add(self, sel: #selector(purchaseFailed), name: .purchaseFailed)
    }
    
    deinit {
        Notificator.remove(self, name: .restored)
        Notificator.remove(self, name: .purchased)
        Notificator.remove(self, name: .purchaseFailed)
    }
}

extension SettingsController {
    
    func checkIfUnlocked() {
        if UserDefaults.unlockedApp, titles.contains(Constants.Labels.unlockApp) {
            titles.removeLast()
        }
    }
    
    private func buyUnlockedApp() {
        let popuper = PopupManager(delegate: self)
        popuper.showPurchasePopup {
            StoreManager.shared.buyUnlockedApp()
        }
    }
    
    @objc private func restoredUpdate() {
        checkIfUnlocked()
        self.tableView.reloadData()
        let popuper = PopupManager(delegate: self)
        popuper.showRestoredPopup()
    }
    
    @objc private func purchasedUpdate() {
        checkIfUnlocked()
        self.tableView.reloadData()
    }
    
    @objc private func purchaseFailed() {
        let popuper = PopupManager(delegate: self)
        popuper.showPurchaseFailed()
    }
}

extension SettingsController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let back = UIBarButtonItem(title: "", style: .done, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = back
    }
}

extension SettingsController {

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.tableView(tableView, didSelectRowAt: indexPath)
        let name = titles[indexPath.row]
        switch name {
        case Constants.Labels.customizeConverters:
            perform(segue: Constants.Segues.customize)
        case Constants.Labels.legal:
            perform(segue: Constants.Segues.legal)
        case Constants.Labels.about:
            perform(segue: Constants.Segues.about)
        case Constants.Labels.restorePurchases:
            StoreManager.shared.restorePurchases()
        case Constants.Labels.unlockApp:
            buyUnlockedApp()
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DetailsCell = tableView.dequeueReusableCell(for: indexPath)
        cell.titleLabel.text = titles[indexPath.row]
        return cell
    }
}
