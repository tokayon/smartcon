//
//  FetchManager.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/1/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation
import CoreData

class FetchManager: NSObject {
    
    private var context: NSManagedObjectContext!
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    private lazy var fetchedResultController: NSFetchedResultsController<ConvertEntity> = {
        let fetchRequest: NSFetchRequest<ConvertEntity> = ConvertEntity.fetchRequest()
        fetchRequest.sortDescriptors = [ NSSortDescriptor(key: #keyPath(ConvertEntity.orderIndex), ascending: true) ]
        let fetchedResultController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultController.delegate = self
        return fetchedResultController
    }()
    
    func fetchConverts() -> Result<[ConvertEntity], Error> {
        let error = Errors.databaseError
        do {
            try fetchedResultController.performFetch()
            guard let convertEntities = fetchedResultController.fetchedObjects
                else { return .failure(error) }
            return .success(convertEntities)
        } catch {
            print("Unable to Execute Fetch Request")
            print("\(error), \(error.localizedDescription)")
            return .failure(error)
        }
    }
    
    func update(_ convert: Convert, with index: Int) {
        print("Update with index: ", index)
        guard let entities = fetchedResultController.fetchedObjects else { return }
        let entityToUpdate = entities.first { $0.name == convert.name }
        let entityToSwap = entities.first { $0.orderIndex == index }
        guard entityToUpdate?.orderIndex != entityToSwap?.orderIndex else { return }
        if let swapIndex = entityToUpdate?.orderIndex {
            entityToSwap?.orderIndex = swapIndex
            entityToUpdate?.orderIndex = Int16(index)
        }
    }
    
    func delete(_ convert: Convert) -> BoolResult {
        let fetchRequest: NSFetchRequest<ConvertEntity> = ConvertEntity.fetchRequest()
        do {
            let entities = try context.fetch(fetchRequest)
            let result = entities.first { $0.name?.lowercased() == convert.name.lowercased() }
            guard let entity = result else { return .failure(.databaseError) }
            context.delete(entity)
            return .success(true)
        } catch {
            return .failure(.databaseError)
        }
    }
    
    func fetchConvertEntity(name: String) -> Result<ConvertEntity?, Errors> {
        let fetchRequest: NSFetchRequest<ConvertEntity> = ConvertEntity.fetchRequest()
        do {
            let entities = try context.fetch(fetchRequest)
            let result = entities.first { $0.name?.lowercased() == name.lowercased() }
            return .success(result)
        } catch {
            return .failure(.databaseError)
        }
    }
    
    func checkDublicate(of convertName: String) -> BoolResult {
        let result = fetchConvertEntity(name: convertName)
        switch result {
        case .success(let entity):
            let existed = entity != nil
            return .success(existed)
        default:
            return .failure(.databaseError)
        }
    }
}

// MARK: - NSFetchedResultsControllerDelegate
extension FetchManager: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            print("Insert")
            Notificator.post(.insert)
        case .delete:
            if let indexPath = indexPath {
                print("Delete index path: ", indexPath)
            }
            Notificator.post(.update)
        case .update:
            if let indexPath = indexPath {
                print("Update index path: ", indexPath)
            }
            print("Post update base")
            Notificator.post(.update)
        case .move:
            if let indexPath = indexPath {
                print("Move index path: ", indexPath)
            }
            
            if let newIndexPath = newIndexPath {
                print("Move new index path: ", newIndexPath)
            }

        @unknown default:
            break
        }
    }
}
