//
//  TipsController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 3/20/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit

class TipsController: BaseTableController {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
}

// MARK: - Setup
extension TipsController {
    func initialSettings() {
        
    }
    
    func configureCell(by indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell: TipCell = tableView.dequeueReusableCell(for: indexPath)
            cell.numberLabel.text = Constants.Labels.firstNumber
            cell.tipLabel.text = Constants.Labels.firstTip
            return cell
        case 1:
            let cell: ExampleCell = tableView.dequeueReusableCell(for: indexPath)
            cell.exampleLabel.text = Constants.Labels.firstTipExample
            return cell
        case 2:
            let cell: TipCell = tableView.dequeueReusableCell(for: indexPath)
            cell.numberLabel.text = Constants.Labels.secondNumber
            cell.tipLabel.text = Constants.Labels.secondTip
            return cell
        case 3:
            let cell: ExampleCell = tableView.dequeueReusableCell(for: indexPath)
            cell.exampleLabel.text = Constants.Labels.secondTipExample
            return cell
        case 4:
            let cell: ButtonCell = tableView.dequeueReusableCell(for: indexPath)
            cell.numberLabel.text = Constants.Labels.thirdNumber
            cell.button.setTitle("check", for: .normal)
            cell.button.layer.cornerRadius = 6
            cell.button.layer.borderWidth = 0.5
            cell.button.layer.borderColor = UIColor.darkGray.cgColor
            cell.button.backgroundColor = UIColor.buttonGreen
            return cell
        case 5:
            let cell: TipCell = tableView.dequeueReusableCell(for: indexPath)
            cell.numberLabel.text = ""
            cell.tipLabel.text = Constants.Labels.thirdTip
            return cell
        case 6:
            let cell: ButtonCell = tableView.dequeueReusableCell(for: indexPath)
            cell.numberLabel.text = Constants.Labels.fourthNumber
            cell.button.setTitle("\u{2B0C}", for: .normal)
            cell.button.titleLabel?.font = UIFont.systemFont(ofSize: 30, weight: .bold)
            cell.button.layer.cornerRadius = 6
            cell.button.layer.borderWidth = 0.5
            cell.button.layer.borderColor = UIColor.darkGray.cgColor
            cell.button.backgroundColor = UIColor.buttonDarkGray
            return cell
        case 7:
            let cell: TipCell = tableView.dequeueReusableCell(for: indexPath)
            cell.numberLabel.text = ""
            cell.tipLabel.text = Constants.Labels.fourthTip
            return cell
        default:
            return UITableViewCell()
        }
    }
}

// MARK: - TableView
extension TipsController {
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 4,6:
            return 50
        default:
            return UITableView.automaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return configureCell(by: indexPath)
    }
}
