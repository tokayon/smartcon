//
//  CoreDataStack.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 5/6/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit
import CoreData

final class CoreDataStack {

    // MARK: - Properties

    private let modelName: String

    let operationQueue = OperationQueue()
    
    private(set) lazy var mainManagedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.parent = self.privateManagedObjectContext
        return managedObjectContext
    }()

    private lazy var privateManagedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        return managedObjectContext
    }()
    
    private lazy var managedObjectModel: NSManagedObjectModel = {
        // Fetch Model URL
        guard let modelURL = Bundle.main.url(forResource: self.modelName, withExtension: "momd") else {
            fatalError("Unable to Find Data Model")
        }

        // Initialize Managed Object Model
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Unable to Load Data Model")
        }

        return managedObjectModel
    }()

    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // Initialize Persistent Store Coordinator
        var persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)

        // Helpers
        let fileManager = FileManager.default
        let storeName = "\(self.modelName).sqlite"

        // URL Documents Directory
        let documentsDirectoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]

        // URL Persistent Store
        let persistentStoreURL = documentsDirectoryURL.appendingPathComponent(storeName)

        do {
            // Add Persistent Store
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: persistentStoreURL, options: [NSMigratePersistentStoresAutomaticallyOption: true,
            NSInferMappingModelAutomaticallyOption: true])
        } catch {
            fatalError("Unable to Add Persistent Store")
        }

        return persistentStoreCoordinator
    }()

    // MARK: - Initialization

    init(modelName: String) {
        self.modelName = modelName
        setupNotificationHandler()
    }
    
    deinit {
        removeNotifications()
    }

    private func setupNotificationHandler() {
        Notificator.add(self, sel: #selector(saveChanges(_:)), name: .terminated)
        Notificator.add(self, sel: #selector(saveChanges(_:)), name: .enterBackground)
        Notificator.add(self, sel: #selector(saveChanges(_:)), name: .save)
    }
    
    private func removeNotifications() {
        Notificator.remove(self, name: .terminated)
        Notificator.remove(self, name: .enterBackground)
        Notificator.remove(self, name: .save)
    }
    
    @objc private func saveChanges(_ notification: Notification) {
        saveChanges()
    }
    
    func saveChanges() {
        mainManagedObjectContext.perform {
            do {
                if self.mainManagedObjectContext.hasChanges {
                    try self.mainManagedObjectContext.save()
                }
            } catch {
                print("Unable to save changes on the main managed object context")
                print("\(error), \(error.localizedDescription)")
            }
            
            self.privateManagedObjectContext.perform {
                do {
                    if self.privateManagedObjectContext.hasChanges {
                        try self.privateManagedObjectContext.save()
                    }
                } catch {
                    print("Unable to save changes on the private managed object context")
                    print("\(error), \(error.localizedDescription)")
                }
            }
        }
    }
    
    func saveAndWait() {
        mainManagedObjectContext.performAndWait {
            do {
                if self.mainManagedObjectContext.hasChanges {
                    try self.mainManagedObjectContext.save()
                    print("main context saved")
                } else {
                    print("no changes to save")
                }
            } catch {
                print("Unable to save changes on the main managed object context")
                print("\(error), \(error.localizedDescription)")
            }
            
            self.privateManagedObjectContext.performAndWait {
                do {
                    if self.privateManagedObjectContext.hasChanges {
                        try self.privateManagedObjectContext.save()
                        print("private context saved")
                    } else {
                        print("no changes to save in private")
                    }
                } catch {
                    print("Unable to save changes on the private managed object context")
                    print("\(error), \(error.localizedDescription)")
                }
            }
        }
    }
    
    func seed() {
        guard let version = UserDefaults.baseVersion, version == Constants.version else {
            let operation = SeedOperation(with: mainManagedObjectContext) { (success) in
                if success {
                    UserDefaults.setBaseVersion(value: Constants.version)
                }
            }
            operationQueue.addOperation(operation)
            return
        }
    }
}

