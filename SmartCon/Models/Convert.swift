//
//  Convert.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 11/24/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit

struct Convert: Codable {
    var permanent = false
    var orderIndex = 0
    var name = ""
    var a = ""
    var b = ""
    var aData: [String: String]?
    var bData: [String: String]?
    var equation: Equation?
}

