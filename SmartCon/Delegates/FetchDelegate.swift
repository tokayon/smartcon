//
//  FetchDelegate.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/1/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

protocol FetchDelegate: class {
    func insert(entity: ConvertEntity, to path: IndexPath)
}

// MARK: - Optional
extension FetchDelegate {
    func insert(entity: ConvertEntity, to path: IndexPath) {}
}
