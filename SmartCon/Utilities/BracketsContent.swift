//
//  BracketsContent.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 1/12/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import Foundation

struct BracketsContent {
    var content: String
    var range: ClosedRange<String.Index>
}
