//
//  Errors.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/1/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

enum Errors: Error {
    case databaseError
}

extension Errors: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .databaseError:
            return NSLocalizedString("Database error occurs", comment: "Errors")
        }
    }
}
