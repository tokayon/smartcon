//
//  DetailsType.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/11/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

enum DetailsType: String {
    case name = "Name of Convert"
    case a = "Left Title (a)"
    case b = "Right Title (b)"
    case equation = "Equation"
    
    var title: String {
        return self.rawValue
    }
    
    static func type(for index: Int) -> DetailsType {
        switch index {
        case 0:
            return .name
        case 1:
            return .a
        case 2:
            return .b
        case 3:
            return .equation
        default:
            return .name
        }
    }
}
