//
//  StoreManager.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 11/11/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation
import StoreKit

class StoreManager: NSObject {
    static let shared = StoreManager()
    private override init() {}
    
    private enum PaymentState {
        case initial
        case buying
        case restoring
    }
    
    private var paymentState: PaymentState = .initial
    var productIDs = [Constants.Store.productID]
    var productsArray = [SKProduct]()
    
    func requestProductInfo() {
        if SKPaymentQueue.canMakePayments() {
            let productIdentifiers = Set(productIDs)
            let productRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
            productRequest.delegate = self
            productRequest.start()
        } else {
            print("Cannot perform In App Purchases.")
        }
    }
}

extension StoreManager: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        guard !response.products.isEmpty else { return }
        for product in response.products {
            productsArray.append(product)
        }
    }
    
    func buyUnlockedApp() {
        guard !productsArray.isEmpty else { return }
        guard let unlockedappProduct = productsArray.first else { return }
        paymentState = .buying
        let payment = SKPayment(product: unlockedappProduct)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(payment)
    }
    
    func restorePurchases() {
        paymentState = .restoring
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

extension StoreManager: SKPaymentTransactionObserver {
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                print("Payment queue purchased")
                complete(transaction: transaction)
                break
            case .failed:
                print("Payment queue failed")
                fail(transaction: transaction)
                break
            case .restored:
                print("Payment queue restored")
                restore(transaction: transaction)
                break
            case .deferred:
                print("Payment queue deferred")
                break
            case .purchasing:
                print("Payment queue purchasing")
                break
            @unknown default:
                break
            }
        }
    }
    
    private func complete(transaction: SKPaymentTransaction) {
      deliverPurchaseNotificationFor(identifier: transaction.payment.productIdentifier)
      SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    private func restore(transaction: SKPaymentTransaction) {
       guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
       deliverPurchaseNotificationFor(identifier: productIdentifier)
       SKPaymentQueue.default().finishTransaction(transaction)
     }
    
    private func fail(transaction: SKPaymentTransaction) {
        SKPaymentQueue.default().finishTransaction(transaction)
        Notificator.post(.purchaseFailed)
    }
    
    private func deliverPurchaseNotificationFor(identifier: String?) {
        guard let productID = identifier, productID == Constants.Store.productID
            else { return }
        UserDefaults.setUnlocked()
        switch paymentState {
        case .restoring:
            Notificator.post(.restored)
        case .buying:
            Notificator.post(.purchased)
        default:
            break
        }
    }
}
 
