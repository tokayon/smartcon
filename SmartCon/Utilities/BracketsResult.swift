//
//  BracketsResult.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 1/11/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import Foundation

enum BracketsResult {
    case success(BracketsContent)
    case failure(EquationError)
}
