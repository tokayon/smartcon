//
//  DetailsCell.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 2/26/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit

class DetailsCell: CustomCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
}
