//
//  DetailsController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/10/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit
import AVFoundation

class DetailsController: BaseTableController {
    
    @IBOutlet weak var textField: EquationTextField!
    @IBOutlet weak var statusLabel: UILabel!
    
    var detailsType: DetailsType = .name
    var input: String = ""
    var callback: ((DetailsResult) -> Void)?
    var showingStatus = false
    var deleteTimer: Timer?
    let manager = EquationManager()

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.textField.becomeFirstResponder()
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        handleCallback()
    }
}

// MARK: - Setup
extension DetailsController {
    func initialSettings() {
        self.title = detailsType.title
        setupTextfield()
        setupStatus()
    }
    
    func handleCallback() {
        if detailsType == .equation {
            let result = DetailsResult(type: detailsType, result: textField.attributedText?.string)
            callback?(result)
        } else {
            let result = DetailsResult(type: detailsType, result: textField.text)
            callback?(result)
        }
    }
    
    func setupTextfield() {
        textField.backgroundColor = UIColor.gray(240)
        switch detailsType {
        case .name:
            textField.tag = Tags.nameField.tag
            textField.text = input
        case .a:
            textField.tag = Tags.abbrField.tag
            textField.text = input
        case .b:
            textField.tag = Tags.abbrField.tag
            textField.text = input
        case .equation:
            textField.reverse = input.contains("a")
            textField.attributedText = input.attributedBlueEquation
            textField.titleIsHidden = false
            textField.tag = Tags.equationField.tag
            textField.inputView = EquationBoard(delegate: self)
        }
    }
    
    func setupStatus() {
        if detailsType == .name {
            let result = DataManager.shared.checkDublicate(of: input)
            switch result {
            case .success(let existed):
                if existed { show(status: Constants.Labels.notAllowed) }
            default:
                break
            }
        }
    }
    
    func show(status: String) {
        statusLabel.text = status
    }
    
    func show(status: String, secs: Double) {
        guard !showingStatus else { return }
        show(status: status)
        showingStatus = true
        DispatchQueue.main.asyncAfter(deadline: .now() + secs) {
            self.statusLabel.text = ""
            self.showingStatus = false
        }
    }
    
    func playKeyboardSound() {
        AudioServicesPlaySystemSound(Constants.clickSound)
    }
    
    func playHepticSound() {
        AudioServicesPlaySystemSound(Constants.hepticSound)
    }
}

// MARK: - EquationBoardDelegate
extension DetailsController: EquationBoardDelegate {
    
    @objc func longDelete(_ gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
            deleteTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { [weak self] (timer) in
                self?.deleteLast()
            })
        } else if gesture.state == .ended || gesture.state == .cancelled {
            deleteTimer?.invalidate()
            deleteTimer = nil
        }
    }
    
    func deleteLast() {
        playKeyboardSound()
        textField.deleteBackward()
    }
    
    func gotoInfo() {
        playKeyboardSound()
        performSegue(withIdentifier: Constants.Segues.tips, sender: nil)
    }
    
    func checkResult() {
        let input = textField.text ?? ""
        let checked = manager.checkAndCreate(equation: input)
        
        if let error = checked.error {
            playHepticSound()
            show(status: error.localizedDescription, secs: 2)
            return
        }
        playKeyboardSound()
        show(status: Constants.Labels.correct, secs: 2)
    }
    
    func proceed(with value: String) {
        let current = textField.text ?? ""
        let reverse = textField.reverse
        let result = manager.check(value: value, after: current, reverse: reverse)
        switch result {
        case .success:
            playKeyboardSound()
            textField.attributedText = Presenter.append(value: value, to: textField.attributedText)
        case .failure(let error):
            playHepticSound()
            show(status: error.localizedDescription, secs: 2)
        }
    }
        
    func reverseValues() {
        playHepticSound()
        textField.reverse = !textField.reverse
        textField.attributedText = Presenter.reverse(equation: textField.attributedText, reverse: textField.reverse)
    }
}

// MARK: - TextField
extension DetailsController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        switch textField.tag {
        case Tags.nameField.tag:
            if count > Constants.nameLength {
                show(status: Constants.Labels.shorterName, secs: 2)
            }
            return count <= Constants.nameLength
        case Tags.abbrField.tag:
            if count > Constants.abbrLength {
                show(status: Constants.Labels.shorterAbbr, secs: 2)
            }
            return count <= Constants.abbrLength
        default:
            return true
        }
    }
}
