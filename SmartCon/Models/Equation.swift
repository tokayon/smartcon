//
//  Equation.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/27/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

struct Equation: Codable {
    var raw: String
    var a: String?
    var b: String?
    
    var isLegal: Bool {
        return !raw.isEmpty
    }
     
    var presented: String {
        return raw.contains("a") ? Constants.aEqualTitle+raw : Constants.bEqualTitle+raw
    }
    
    init() {
        self.raw = ""
        self.a = ""
        self.b = ""
    }
    
    init(raw: String, a: String? = nil, b: String? = nil) {
        self.raw = raw
        self.a = a
        self.b = b
    }
}
