//
//  WebController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 3/28/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit
import WebKit

class WebController: UIViewController {
    
    var webView: WKWebView!
    var path: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        overrideUserInterfaceStyle = .dark
        showUrl()
    }
    
    override func loadView() {
        webView = WKWebView()
        view = webView
    }
    
    private func showUrl() {
        guard let path = path else { return }
        guard let url = URL(string: path) else { return }
        webView.load(URLRequest(url: url))
    }
}
