//
//  PointCell.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/9/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit

class PointCell: CustomCell {
    
    @IBOutlet weak var pointView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
}
