//
//  HalfViewDelegate.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 11/28/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

protocol HalfViewDelegate: class {
    func updated(tag: TextfieldTag, text: String)
}
