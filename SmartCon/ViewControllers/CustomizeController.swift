//
//  CustomizeController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 7/29/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit
import CoreData

class CustomizeController: BaseTableController {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var addItem: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var dragInitialIndexPath: IndexPath?
    var dragCellSnapshot: UIView?
    var converts: [Convert] = []

    let dataManager = DataManager.shared
    
    override func viewDidLoad() {
        self.title = Constants.Labels.customize
        initialSettings()
        subscribeNotifications()
    }

    @IBAction func add(_ sender: UIBarButtonItem) {
        guard UserDefaults.unlockedApp else {
            buyUnlockedApp()
            return
        }
        addCustomConvert()
    }
       
    deinit {
        unsubscribeNotifications()
    }
}

// MARK: - Notifications
extension CustomizeController {
    
    func subscribeNotifications() {
        Notificator.add(self, sel: #selector(updateConverts), name: .update)
        Notificator.add(self, sel: #selector(insertConvert), name: .insert)
        Notificator.add(self, sel: #selector(purchaseFailed), name: .purchaseFailed)
    }
    
    func unsubscribeNotifications() {
        Notificator.remove(self, name: .update)
        Notificator.remove(self, name: .insert)
        Notificator.remove(self, name: .purchaseFailed)
    }
}

// MARK: - Setup
private extension CustomizeController {
    
    func initialSettings() {
        let longTap = UILongPressGestureRecognizer(target: self, action: #selector(tapLongPress(sender:)))
        longTap.minimumPressDuration = 0.3
        self.view.addGestureRecognizer(longTap)
        fetchConverts()
    }
    
    @objc func insertConvert(_ notification: Notification) {
        fetchConverts()
    }
    
    @objc func updateConverts(_ notification: Notification) {
        fetchConverts()
    }
    
    @objc private func purchaseFailed() {
        let popuper = PopupManager(delegate: self)
        popuper.showPurchaseFailed()
    }
    
    func fetchConverts() {
        let result = dataManager.fetchConverts()
        switch result {
        case .success(let convertEntities):
            converts.removeAll()
            for entity in convertEntities {
                guard let convert = dataManager.createConvert(from: entity)
                    else { return }
                converts.append(convert)
            }
            self.tableView.reloadData()
        case .failure(let error):
            print("Handle error", error.localizedDescription)
        }
    }
    
    func buyUnlockedApp() {
        let popuper = PopupManager(delegate: self)
        popuper.showPurchasePopup {
            StoreManager.shared.buyUnlockedApp()
        }
    }
    
    func addCustomConvert() {
        let navVC: NavController = instantiateViewController()
        self.present(navVC, animated: true, completion: nil)
    }
}

// MARK: - TableView
extension CustomizeController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Constants.Segues.convertDetails, sender: indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return converts.count
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }

    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DetailsCell = tableView.dequeueReusableCell(for: indexPath)
        cell.titleLabel.text = converts[indexPath.row].name
        return cell
    }
}

// MARK: - Drap and Drop
extension CustomizeController {
    @objc func tapLongPress(sender: UILongPressGestureRecognizer) {
        let locationInView = sender.location(in: tableView)
        let indexPath = tableView.indexPathForRow(at: locationInView)

        switch sender.state {
        case .began:
            guard let path = indexPath else { return }
            dragInitialIndexPath = path
            guard let cell = tableView.cellForRow(at: path) else { return }
            let snapshot = snapshotOfCell(inputView: cell)
            var center = cell.center
            snapshot.center = center
            snapshot.alpha = 0.0
            tableView.addSubview(snapshot)
            dragCellSnapshot = snapshot
            UIView.animate(withDuration: 0.25) {
                center.y = locationInView.y
                self.dragCellSnapshot?.center = center
                if let transform = self.dragCellSnapshot?.transform.scaledBy(x: 1.05, y: 1.05) {
                    self.dragCellSnapshot?.transform = transform
                }
                self.dragCellSnapshot?.alpha = 0.99
                cell.isHidden = true
            }
        case .changed:
            guard let path = indexPath else { return }
            guard let dragPath = dragInitialIndexPath else { return }
            guard let snapshot = dragCellSnapshot else { return }
            var center = snapshot.center
            center.y = locationInView.y
            dragCellSnapshot?.center = center

            if path != dragPath {
                var convert = converts.remove(at: dragPath.row)
                converts.insert(convert, at: path.row)
                tableView.moveRow(at: dragPath, to: path)
                dragInitialIndexPath = indexPath
                dataManager.update(convert, with: path.row)
                convert.orderIndex = path.row
            }
        case .ended:
            guard let dragPath = dragInitialIndexPath else { return }
            guard let cell = tableView.cellForRow(at: dragPath) else { return }
            cell.isHidden = false
            cell.alpha = 0.0
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
              self.dragCellSnapshot?.center = cell.center
              self.dragCellSnapshot?.transform = CGAffineTransform.identity
              self.dragCellSnapshot?.alpha = 0.0
              cell.alpha = 1.0
            }, completion: { (finished) -> Void in
              if finished {
                self.dragInitialIndexPath = nil
                self.dragCellSnapshot?.removeFromSuperview()
                self.dragCellSnapshot = nil
              }
            })
        default:
            break
        }
    }
    
    func snapshotOfCell(inputView: UIView) -> UIView {
        UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, false, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else { return UIView()}
        inputView.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        let cellSnapshot = UIImageView(image: image)
        cellSnapshot.layer.masksToBounds = false
        cellSnapshot.layer.cornerRadius = 0.0
        cellSnapshot.layer.shadowOffset = CGSize(width: -5.0, height: 0.0)
        cellSnapshot.layer.shadowRadius = 5.0
        cellSnapshot.layer.shadowOpacity = 0.4
        return cellSnapshot
    }
}


// MARK: - Navigation
extension CustomizeController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let dest = segue.destination as? ConvertDetailsController else { return }
        guard let index = sender as? Int else { return }
        guard converts.count > index else { return }
        dest.convert = converts[index]
    }
}
