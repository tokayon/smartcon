//
//  CustomCell.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 7/29/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let view = UIView()
        view.backgroundColor = UIColor.cellSelected
        selectedBackgroundView? = view
    }
}
