//
//  ValueButton.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/17/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit

class ValueButton: UIButton {
    var value: String = ""
    
    var width = 1.0
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: width, height: 1.0)
    }
}
