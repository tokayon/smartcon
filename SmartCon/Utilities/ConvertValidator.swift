//
//  ConvertValidator.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/30/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

class ConvertValidator {
    
    class func nameIsExisted(for convert: Convert) -> BoolResult {
        return DataManager.shared.checkDublicate(of: convert.name)
    }
    
    class func nameIsValid(for convert: Convert) -> Bool {
        return !convert.name.isEmpty && convert.name.count <= Constants.nameLength
    }
    
    class func aIsValid(for convert: Convert) -> Bool {
        return !convert.a.isEmpty && convert.a.count <= Constants.abbrLength
    }
    
    class func bIsValid(for convert: Convert) -> Bool {
        return !convert.b.isEmpty && convert.b.count <= Constants.abbrLength
    }
    
    class func equationIsValid(for convert: Convert) -> Bool {
        convert.equation?.a != nil
    }
    
    class func convertIsValid(_ convert: Convert) -> Bool {
        let nameOK = nameIsValid(for: convert)
        let aOK = aIsValid(for: convert)
        let bOK = bIsValid(for: convert)
        let equationOK = equationIsValid(for: convert)
        return nameOK && aOK && bOK && equationOK
    }
}
