//
//  LegalController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 3/28/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit

class LegalController: BaseTableController {
   
    @IBOutlet weak var tableView: UITableView!
    
    var titles = [Constants.Labels.privacy, Constants.Labels.terms]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
}

// MARK: - Setup
extension LegalController {
    func initialSettings() {
        self.title = Constants.Labels.legal
    }
}

// MARK: - TableView
extension LegalController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let webController = WebController()
        if indexPath.row == 0 {
            webController.path = Constants.privacyUrl
        } else if indexPath.row == 1 {
            webController.path = Constants.termsUrl
        }
        self.present(webController, animated: true, completion: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DetailsCell = tableView.dequeueReusableCell(for: indexPath)
        cell.titleLabel.text = titles[indexPath.row]
        return cell
    }
}
