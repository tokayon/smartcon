//
//  CheckedTitle.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 1/5/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit

struct CheckedTitle {
    var title: NSAttributedString
    var pointColor: UIColor
    
    init() {
        self.title = NSAttributedString()
        self.pointColor = UIColor.clear
    }
}
