//
//  EquationError.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 1/9/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import Foundation

enum EquationError: Error {
    case wrongEquation
    case wrongOperandsOrder
    case wrongOperatorOrder
    case zeroDivision
    case doubleOperand
    case notFullEquation
    case emptyEquation
    case saveError
    case empty
}

extension EquationError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .wrongEquation:
            return NSLocalizedString("Wrong equation", comment: "EquationErrors")
        case .wrongOperatorOrder:
            return NSLocalizedString("Use operand after operator", comment: "EquationErrors")
        case .wrongOperandsOrder:
            return NSLocalizedString("Use operator after operand", comment: "EquationErrors")
        case .zeroDivision:
            return NSLocalizedString("Division by zero not allowed", comment: "EquationErrors")
        case .doubleOperand:
            return NSLocalizedString("This operand exists in equation", comment: "EquationErrors")
        case .notFullEquation:
            return NSLocalizedString("Not a full equation", comment: "EquationErrors")
        case .saveError:
            return NSLocalizedString("Convert can't be saved", comment: "EquationErrors")
        case .emptyEquation:
            return NSLocalizedString("Empty equation", comment: "EquationErrors")
        case .empty:
            return NSLocalizedString("", comment: "EquationErrors")
        }
    }
}
