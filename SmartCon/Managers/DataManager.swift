//
//  DataManager.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/2/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation
import CoreData

class DataManager: NSObject {
    
    static let shared = DataManager()
    private var fetcher: FetchManager!
    
    private var coredataManager = CoreDataStack(modelName: Constants.modelName)
    
    var context: NSManagedObjectContext {
        return coredataManager.mainManagedObjectContext
    }
    
    private override init() {
        super.init()
        fetcher = FetchManager(context: context)
    }
}

extension DataManager {
    
    func saveAndWait() {
        self.coredataManager.saveAndWait()
    }
    
    func saveChanges() {
        self.coredataManager.saveChanges()
    }
    
    func seed() {
        self.coredataManager.seed()
    }
    
    func fetchConverts() -> Result<[ConvertEntity], Error> {
        return fetcher.fetchConverts()
    }
    
    func delete(_ convert: Convert) -> BoolResult {
        let result = fetcher.delete(convert)
        saveAndWait()
        return result
    }
    
    func update(_ convert: Convert, with index: Int) {
        fetcher.update(convert, with: index)
        saveAndWait()
    }
    
    func checkDublicate(of convertName: String) -> BoolResult {
        return fetcher.checkDublicate(of: convertName)
    }

    func fetchAndCreateConvert(name: String) -> Convert? {
        let fetchResult = fetcher.fetchConvertEntity(name: name)
        switch fetchResult {
        case .success(let entity):
            guard let entity = entity else { return nil }
            return createConvert(from: entity)
        case .failure(_):
            return nil
        }
    }
    
    func createConvert(from entity: ConvertEntity) -> Convert? {
        guard let name = entity.name else { return nil }
        guard let a = entity.a else { return nil }
        guard let b = entity.b else { return nil }
        guard let rawEquation = entity.rawEquation else { return nil }
        guard let aEquation = entity.aEquation else { return nil }
        guard let bEquation = entity.bEquation else { return nil }

        var convert = Convert()
        convert.name = name
        convert.permanent = entity.permanent
        convert.orderIndex = Int(entity.orderIndex)
        convert.a = a
        convert.b = b
        convert.equation = Equation(raw: rawEquation, a: aEquation, b: bEquation)
        
        if let aData = entity.aData {
            do {
                convert.aData = try JSONDecoder().decode([String:String].self, from: aData)
            } catch {
                print(error.localizedDescription)
            }
        }
        
        if let bData = entity.bData {
            do {
                convert.bData = try JSONDecoder().decode([String:String].self, from: bData)
            } catch {
                print(error.localizedDescription)
            }
        }
        return convert
    }
    
    func save(convert: Convert, _ completion: ((CheckResult) -> Void)? = nil) {
        guard let count = checkAmount(), let entity = createEmptyConvertEntity() else {
            completion?(.failure(.saveError))
            return
        }
        entity.name = convert.name
        entity.a = convert.a
        entity.b = convert.b
        entity.orderIndex = Int16(count)
        if let equation = convert.equation {
            entity.rawEquation = equation.raw
            entity.aEquation = equation.a
            entity.bEquation = equation.b
        }
        
        self.coredataManager.saveAndWait()
        completion?(.success)
    }
    
    func update(convert: Convert, _ completion: ((CheckResult) -> Void)? = nil) {
        let fetchResult = fetcher.fetchConvertEntity(name: convert.name)
        switch fetchResult {
        case .success(let entity):
            guard let entity = entity else {
                completion?(.failure(.saveError))
                return
            }
            entity.a = convert.a
            entity.b = convert.b
            if let equation = convert.equation {
                entity.rawEquation = equation.raw
                entity.aEquation = equation.a
                entity.bEquation = equation.b
            }
            self.coredataManager.saveAndWait()
            completion?(.success)
        case .failure(_):
            completion?(.failure(.saveError))
        }
    }
}

private extension DataManager {
    private func createEmptyConvertEntity() -> ConvertEntity? {
        guard let entity = NSEntityDescription.entity(forEntityName: Constants.entityName, in: context) else { return nil }
        return ConvertEntity(entity: entity, insertInto: context)
    }
    
    private func checkAmount() -> Int? {
        let fetchRequest: NSFetchRequest<ConvertEntity> = ConvertEntity.fetchRequest()
        do {
            let entities = try context.fetch(fetchRequest)
            print("Entities count: ", entities.count)
            return entities.count
        } catch {
            return nil
        }
    }
}
