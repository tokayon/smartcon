//
//  Seed.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 11/24/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

struct Seed: Codable {
    let converts: [Convert]
}
