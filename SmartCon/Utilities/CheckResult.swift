//
//  CheckResult.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 1/13/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import Foundation

enum CheckResult {
    case success
    case failure(EquationError)
}
