//
//  PopupController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 3/14/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit

class PopupController: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var divider: UIView!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    var message: String?
    var firstTitle: String?
    var secondTitle: String?
    var firstAction: (() -> Void)?
    var secondAction: (()-> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showPopup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        hidePopup()
    }
    
    @IBAction func firstButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: firstAction)
    }
    
    @IBAction func secondButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: secondAction)
    }
}

extension PopupController {
    func initialSettings() {
        if let message = message {
            messageLabel.text = message
        } else {
            messageLabel.removeFromSuperview()
            divider.isHidden = true
        }
        
        if let first = firstTitle {
            firstButton.setTitle(first, for: .normal)
        } else {
            firstButton.removeFromSuperview()
            divider.isHidden = true
        }
        
        if let second = secondTitle {
            secondButton.setTitle(second, for: .normal)
        } else {
            secondButton.removeFromSuperview()
        }
    }
    
    func showPopup() {
        bottomConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func hidePopup() {
        bottomConstraint.constant = -150
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}
