//
//  EquationBoardDelegate.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/20/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit

@objc protocol EquationBoardDelegate: class {
    @objc func longDelete(_ gesture: UILongPressGestureRecognizer)
    func deleteLast()
    func gotoInfo()
    func checkResult()
    func proceed(with value: String)
    func reverseValues()
}
