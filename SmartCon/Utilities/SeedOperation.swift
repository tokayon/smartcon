//
//  SeedOperation.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 11/24/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation
import CoreData

class SeedOperation: Operation {
    
    enum SeedError: Error {
        case seedDataNotFound
    }
    
    typealias SeedOperationCompletion = ((Bool)->Void)
    
    // MARK: - Properties
    private let privateManagedObjectContext: NSManagedObjectContext
    private let completion: SeedOperationCompletion?
    
    // MARK: - Initialization
    init(with managedObjectContext: NSManagedObjectContext, completion: SeedOperationCompletion? = nil) {
        // Initialize Managed Object Context
        privateManagedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)

        // Configure Managed Object Context
        privateManagedObjectContext.parent = managedObjectContext
        self.completion = completion
        super.init()
    }
    
    // MARK: - Overrides
    override func main() {
        // Seed With Data
        do {
            try seed()
            completion?(true)
        } catch {
            print("Unable to Save Main Managed Object Context After Seeding Persistent Store (\(error))")
            completion?(false)
        }
    }
    
    // MARK: - Helper Methods
    
    private func seed() throws {
        
        guard let url = Bundle.main.url(forResource: "seed", withExtension: "json") else {
            throw SeedError.seedDataNotFound
        }

        // Load Data
        let data = try Data(contentsOf: url)
        
        // Initialize JSON Decoder
        let decoder = JSONDecoder()
        // Configure JSON Decoder
        decoder.dateDecodingStrategy = .secondsSince1970
        
        // Decode Seed Data
        let seed = try decoder.decode(Seed.self, from: data)
        
        let fetcher = FetchManager(context: privateManagedObjectContext)
        let result = fetcher.fetchConverts()
        var names: [String] = []
        switch result {
        case .success(let entities):
            names = entities.map({$0.name ?? ""})
        default:
            break
        }
        
        for convert in seed.converts {
            if !(names.contains(convert.name)) {
                try save(convert: convert)
            }
        }
        
        Notificator.post(.save)
    }
    
    private func save(convert: Convert) throws {
        let convertEntity = ConvertEntity(context: privateManagedObjectContext)
        convertEntity.name = convert.name
        convertEntity.orderIndex = Int16(convert.orderIndex)
        convertEntity.a = convert.a
        convertEntity.b = convert.b
        
        if let equation = convert.equation {
            convertEntity.rawEquation = equation.raw
            convertEntity.aEquation = equation.a
            convertEntity.bEquation = equation.b
        }
        
        if let aData = convert.aData {
            do {
                convertEntity.aData = try JSONEncoder().encode(aData)
            } catch {
                print(error.localizedDescription)
            }
        }
        
        if let bData = convert.bData {
            do {
                convertEntity.bData = try JSONEncoder().encode(bData)
            } catch {
                print(error.localizedDescription)
            }
        }
        
        convertEntity.permanent = convert.permanent
        try privateManagedObjectContext.save()
    }
}
