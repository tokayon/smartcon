//
//  ButtonCell.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 3/21/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit

class ButtonCell: UITableViewCell {
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var button: UIButton!
}
