//
//  ExampleCell.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 3/21/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit

class ExampleCell: UITableViewCell {
    @IBOutlet weak var exampleLabel: UILabel!
}
