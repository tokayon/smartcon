//
//  AddController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/3/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit

class AddController: BaseTableController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var titles = [DetailsType.name, DetailsType.a, DetailsType.b, DetailsType.equation]
    
    var addEnabled = false
    var convert: Convert?
    var dataManager = DataManager.shared
    var addNew = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
}

// MARK: - Setup
private extension AddController {
    
    func initialSettings() {
        addNew = convert == nil
        self.title = addNew ? Constants.Labels.newConvert : Constants.Labels.editConvert
        convert = addNew ? Convert() : convert
        addButtons()
        checkIfConvertIsValid()
    }
    
    func addButtons() {
        let cancel = UIBarButtonItem(title: Constants.Labels.capCancel, style: .plain, target: self, action: #selector(cancelAdding))
        let add = UIBarButtonItem(title: Constants.Labels.capAdd, style: .done, target: self, action: #selector(saveConvert))
        let done = UIBarButtonItem(title: Constants.Labels.done, style: .done, target: self, action: #selector(editConvert))
        navigationItem.leftBarButtonItem = cancel
        navigationItem.rightBarButtonItem = addNew ? add : done
        navigationItem.rightBarButtonItem?.isEnabled = addEnabled
    }
}

// MARK: - Actions
@objc extension AddController {
    func saveConvert() {
        guard let convert = convert else { return }
        dataManager.save(convert: convert) { [weak self] (result) in
            self?.handle(check: result)
        }
    }
    
    func cancelAdding() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func editConvert() {
        guard let convert = convert else { return }
        dataManager.update(convert: convert) { [weak self] (result) in
            self?.handle(check: result)
        }
    }
}

// MARK: - Helpers
private extension AddController {
    
    func checkIfConvertIsValid() {
        guard let convert = convert else { return }
        let convertIsValid = ConvertValidator.convertIsValid(convert)
        if addNew {
            let result = ConvertValidator.nameIsExisted(for: convert)
            switch result {
            case .success(let existed):
                let isValid = convertIsValid && !existed
                navigationItem.rightBarButtonItem?.isEnabled = isValid
            case .failure(let error):
                print("Handle error: ", error.localizedDescription)
            }
        } else {
            navigationItem.rightBarButtonItem?.isEnabled = convertIsValid
        }
    }
    
    func handle(details: DetailsResult) {
        guard let output = details.result else { return }
        indicator.startAnimating()
        tableView.isUserInteractionEnabled = false
        navigationItem.rightBarButtonItem?.isEnabled = false
        let manager = EquationManager()
        switch details.type {
        case .name:
            convert?.name = output
        case .a:
            convert?.a = output
        case .b:
            convert?.b = output
        case .equation:
            let result = manager.checkAndCreate(equation: output)
            if result.error == .emptyEquation {
                convert?.equation = nil
            } else {
                convert?.equation = result.equation
            }
        }
        indicator.stopAnimating()
        tableView.isUserInteractionEnabled = true
        checkIfConvertIsValid()
        self.tableView.reloadData()
    }
    
    func handle(check result: CheckResult) {
        switch result {
        case .failure(let error):
            let popuper = PopupManager(delegate: self)
            popuper.showPopup(message: error.localizedDescription)
        case .success:
            let impact = UIImpactFeedbackGenerator()
            impact.impactOccurred()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func setup(cell: PointCell, for row: Int) {
        guard let convert = convert else { return }
        let checkedTitle = Presenter.checkedTitle(for: convert, at: row)
        cell.titleLabel.attributedText = checkedTitle.title
        cell.pointView.backgroundColor = checkedTitle.pointColor
        
        if row == 0 {
            cell.accessoryType = addNew ? .disclosureIndicator : .none
            cell.pointView.backgroundColor = addNew ? checkedTitle.pointColor : UIColor.pointYellow
        }
    }
}

// MARK: - TableView
extension AddController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.tableView(tableView, didSelectRowAt: indexPath)
        if indexPath.row == 0 && !addNew { return }
        performSegue(withIdentifier: Constants.Segues.details, sender: indexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PointCell = tableView.dequeueReusableCell(for: indexPath)
        setup(cell: cell, for: indexPath.row)
        return cell
    }
}

extension AddController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let detailsVC = segue.destination as? DetailsController else { return }
        guard let row = sender as? Int else { return }
        guard let convert = convert else { return }
        let detailsType = DetailsType.type(for: row)
        detailsVC.detailsType = detailsType
        detailsVC.callback = handle(details:)
        switch detailsType {
        case .name:
            detailsVC.input = convert.name
        case .a:
            detailsVC.input = convert.a
        case .b:
            detailsVC.input = convert.b
        case .equation:
            detailsVC.input = convert.equation?.raw ?? ""
        }
    }
}
