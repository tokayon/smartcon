//
//  EquationBoard.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/16/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit

class EquationBoard: UIView {
    
    weak var delegate: EquationBoardDelegate?
    
    private var rows = 6
    private var columns = 4
   
    private enum ButtonTitles: String, CaseIterable {
        case info = "i"
        case a = "a"
        case b = "b"
        case check = "check"
        case reverse = "\u{2B0C}"
        case left = "("
        case right = ")"
        case divide = "÷"
        case one = "1"
        case two = "2"
        case three = "3"
        case multiply = "x"
        case four = "4"
        case five = "5"
        case six = "6"
        case minus = "-"
        case seven = "7"
        case eight = "8"
        case nine = "9"
        case plus = "+"
        case dot = "."
        case zero = "0"
        case delete = "⌫"
        
        static func type(for index: Int) -> ButtonTitles? {
            guard ButtonTitles.allCases.count > index else { return nil }
            return ButtonTitles.allCases[index]
        }

        static func value(for index: Int) -> String? {
            return ButtonTitles.type(for: index)?.rawValue
        }
    }
    
    init(delegate: EquationBoardDelegate) {
        self.delegate = delegate
        super.init(frame: .zero)
        configure()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

// MARK: - Actions
extension EquationBoard {
    
    @objc func valueButtonPressed(_ sender: ValueButton) {
        guard let type = ButtonTitles.type(for: sender.tag) else { return }
        switch type {
        case .delete:
            delegate?.deleteLast()
        case .info:
            delegate?.gotoInfo()
        case .check:
            delegate?.checkResult()
        case .reverse:
            delegate?.reverseValues()
        default:
            delegate?.proceed(with: sender.value)
        }
    }
}


// MARK: - Private initial configuration methods
private extension EquationBoard {
    func configure() {
        frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height / 2)
        backgroundColor = UIColor.spaceGray
        addButtons()
    }

    func addButtons() {
        let stackView = createStackView(axis: .vertical)
        stackView.frame = bounds
        stackView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        stackView.spacing = 8
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5)

        addSubview(stackView)

        for row in 1...rows {
            let subStackView = createStackView(axis: .horizontal)
            subStackView.spacing = 8
            stackView.addArrangedSubview(subStackView)
            let width = Double(UIScreen.main.bounds.size.width)

            for column in 1...columns {
                let index = (row - 1) * columns + column - 1
                let button = createValueButton(for: index, width: 1.0)
                let buttonWidth = column % 2 == 0 ? width / 2 : width / 4
                button.width = buttonWidth
                if row == rows, column == columns { continue }
                if row == rows, column == columns - 1 {
                    let longPress = UILongPressGestureRecognizer(target: self.delegate, action: #selector(delegate?.longDelete(_:)))
                    button.addGestureRecognizer(longPress)
                }
                subStackView.addArrangedSubview(button)
            }
            if row == rows {
                subStackView.distribution = .fillProportionally
            }
        }
    }

    func createStackView(axis: NSLayoutConstraint.Axis) -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = axis
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        return stackView
    }
    
    func createValueButton(for index: Int, width: Double) -> ValueButton {
        let button = ValueButton(type: .system)
        button.width = width
        let value = configureButtonTitle(for: index)
        button.value = value ?? ""
        button.tag = index
        button.setTitle(value, for: .normal)
        button.titleLabel?.font = configureButtonFont(for: index)
        button.backgroundColor = configureButtonColor(for: index)
        button.setTitleColor(.white, for: .normal)
        button.layer.borderWidth = 0.5
        button.layer.cornerRadius = 6
        button.layer.borderColor = UIColor.darkGray.cgColor
        button.accessibilityTraits = [.keyboardKey]
        button.addTarget(self, action: #selector(valueButtonPressed(_:)), for: .touchUpInside)
        return button
    }
    
    func configureButtonTitle(for index: Int) -> String? {
        return ButtonTitles.value(for: index)
    }
    
    func configureButtonColor(for index: Int) -> UIColor {
        let defaultColor = UIColor.buttonGray
        guard let type = ButtonTitles.type(for: index) else { return defaultColor }
        switch type {
        case .a, .b:
            return UIColor.buttonBlue
        case .info:
            return UIColor.buttonOrange
        case .check:
            return UIColor.buttonGreen
        case .left, .right, .reverse, .delete, .divide, .multiply, .minus, .plus:
            return UIColor.buttonDarkGray
        default:
            return defaultColor
        }
    }
    
    func configureButtonFont(for index: Int) -> UIFont {
        let defaultFont = UIFont.preferredFont(forTextStyle: .largeTitle)
        guard let type = ButtonTitles.type(for: index) else { return defaultFont }
        switch type {
        case .check, .delete:
            return UIFont.preferredFont(forTextStyle: .title2)
        case .left, .right, .divide, .multiply, .minus, .plus:
            return UIFont.preferredFont(forTextStyle: .title1)
        case .info:
            return UIFont.systemFont(ofSize: 26, weight: .semibold)
        default:
            return defaultFont
        }
    }
}
