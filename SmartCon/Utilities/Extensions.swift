//
//  Extensions.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 5/8/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit
import CoreGraphics
import Darwin

extension String {
    
    var doubled: Double {
        if let value = Double(self) {
            return value
        } else {
            return 0
        }
    }
    
    var isSign: Bool {
        let signs = ["+","-","x","÷","*","/"]
        return signs.contains(self)
    }
    
    var isA: Bool {
        return self == "a"
    }
    
    var isB: Bool {
        return self == "b"
    }
    
    var isAorB: Bool {
        return self == "a" || self == "b"
    }
    
    var isLBracket: Bool {
        return self == "("
    }
    
    var isRBracket: Bool {
        return self == ")"
    }
    
    var isBracket: Bool {
        let brackets = ["(",")"]
        return brackets.contains(self)
    }
    
    var isDot: Bool {
        return self == "."
    }
    
    var striked: NSAttributedString {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
    var attributed: NSAttributedString {
        return NSAttributedString(string: self)
    }
    
    var attributedWhiteEquation: NSAttributedString {
        let signAttrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 19.0),
            NSAttributedString.Key.foregroundColor : UIColor.equationGray]
        let regAttrs = [NSAttributedString.Key.foregroundColor : UIColor.equationWhite]
        let attributed = NSMutableAttributedString()
        for char in self {
            let attrs = char.string.isSign ? signAttrs : regAttrs
            let attrValue = NSAttributedString(string: char.string, attributes: attrs)
            attributed.append(attrValue)
        }
        return attributed
    }
    
    var attributedBlueEquation: NSAttributedString {
        let signAttrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 19.0),
            NSAttributedString.Key.foregroundColor : UIColor.equationOrange]
        let regAttrs = [NSAttributedString.Key.foregroundColor : UIColor.equationBlue]
        let attributed = NSMutableAttributedString()
        for char in self {
            let attrs = char.string.isSign ? signAttrs : regAttrs
            let attrValue = NSAttributedString(string: char.string, attributes: attrs)
            attributed.append(attrValue)
        }
        return attributed
    }
    
    var inted: Int {
        if let value = Int(self) {
            return value
        } else {
            return 0
        }
    }
    
    func precision(_ decimals: Int) -> String {
        if decimals < 0 {
            return ""
        } else if decimals == 0 {
            return "\(Int(self.doubled))"
        } else {
            let divisor = 10.0.pow(Double(decimals))
            let value = (self.doubled*divisor).rounded() / divisor
            let result = String(format: "%.\(decimals)f", value)
            if let last = result.last, let intValue = Int(String(last)), intValue == 0 {
                return result.dropLast().description
            } else {
                return result
            }
        }
    }
    
    func indexOf(str: String) -> Int? {
        guard let char = str.first else { return nil }
        return firstIndex(of: char)?.utf16Offset(in: self)
    }
    
    subscript(value: CountableClosedRange<Int>) -> Substring {
      self[index(at: value.lowerBound)...index(at: value.upperBound)]
    }

    subscript(value: CountableRange<Int>) -> Substring {
      self[index(at: value.lowerBound)..<index(at: value.upperBound)]
    }

    subscript(value: PartialRangeUpTo<Int>) -> Substring {
      self[..<index(at: value.upperBound)]
    }

    subscript(value: PartialRangeThrough<Int>) -> Substring {
      self[...index(at: value.upperBound)]
    }

    subscript(value: PartialRangeFrom<Int>) -> Substring {
      self[index(at: value.lowerBound)...]
    }
    
    subscript(value: Int) -> String {
        guard value >= 0, value < self.count else { return "" }
        let index = String.Index(utf16Offset: value, in: self)
        return String(self[index])
    }
    
    func index(at offset: Int) -> String.Index {
      index(startIndex, offsetBy: offset)
    }
    
}

extension Character {
    var string: String {
        return String(self)
    }
}

extension Double {
    
    var string: String {
        return String(format: "%f", self)
    }
    
    func pow(_ exponent:Double) -> Double {
        return Darwin.pow(self, exponent)
    }
}

extension Notification.Name {
    static let databaseError = Notification.Name("DatabaseError")
    static let databaseSaved = Notification.Name.NSManagedObjectContextDidSave
    static let restored = Notification.Name("Restored")
    static let purchased = Notification.Name("Purchased")
    static let terminated = UIApplication.willTerminateNotification
    static let enterBackground = UIApplication.didEnterBackgroundNotification
    static let save = Notification.Name("SaveConverts")
    static let update = Notification.Name("UpdateConverts")
    static let insert = Notification.Name("InsertConverts")
    static let purchaseFailed = Notification.Name("PurchaseFailed")
}

protocol ReusableView {
    static var reuseIdentifier: String { get }
}

extension ReusableView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableViewCell: ReusableView {}

extension UITableView {
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier) as? T else { fatalError("Unable to Dequeue Reusable Table View Cell") }
        return cell
    }
    
    func cell<T: UITableViewCell>(withID identifier: String) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: identifier) as? T
            else { fatalError("Unable to Dequeue Reusable Table View Cell") }
        return cell
    }
}

extension UIColor {
    static let customBlue = hexColor("4B5D7D")
    static let cellSelected = hexColor("46577D")
    static let grayBlue = hexColor("7C92BB")
    static let darkGrayBlue = hexColor("4B5C84")
    static let tableHeader = hexColor("F0F0F0")
    static let pointDark = hexColor("434343")
    static let pointYellow = hexColor("FBC539")
    static let pointRed = hexColor("E5374C")
    static let spaceGray = hexColor("515456")
    static let buttonGray = hexColor("7D7C82")
    static let buttonDarkGray = hexColor("6A696F")
    static let buttonBlue = hexColor("3F7ABE")
    static let buttonOrange = hexColor("DA933E")
    static let buttonGreen = hexColor("3CA68E")
    static let equationOrange = hexColor("E38E04")
    static let equationGray = hexColor("D8D8D8")
    static let equationBlue = hexColor("4B5C84")
    static let equationWhite = hexColor("F4FAFF")
    
    static func hexColor(_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        if (cString.hasPrefix("#")) { cString.remove(at: cString.startIndex) }
        if cString.count == 6 {
            return sixHexColor(hex: cString, alpha: 1.0)
        } else {
            return UIColor.gray
        }
    }
    
    static func gray(_ tone: CGFloat) -> UIColor {
        return color(red: tone, green: tone, blue: tone, alpha: 1.0)
    }
    
    static func color(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }

    static func sixHexColor(hex: String, alpha: CGFloat) -> UIColor {
        var rgbValue:UInt64 = 0
        Scanner(string: hex).scanHexInt64(&rgbValue)
        let red = CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0
        let green = CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0
        let blue = CGFloat(rgbValue & 0x0000FF) / 255.0
        return UIColor(red: red,green: green, blue: blue, alpha: alpha)
    }
}

extension UIViewController {
    func perform(segue: String) {
        performSegue(withIdentifier: segue, sender: nil)
    }
    
    func instantiateViewController<T>() -> T {
        let identifier = String(describing: T.self)
        guard let vc = self.storyboard?.instantiateViewController(withIdentifier: identifier) as? T else {
            fatalError("No viewController instantiated")
        }
        return vc
    }
    
    func instantiateChildController<T>() -> T {
        guard let child = self.children[0] as? T else {
            fatalError("No viewController instantiated")
        }
        return child
    }
}

extension UserDefaults {

    class func setUnlocked() {
        UserDefaults.standard.set(true, forKey: Constants.Store.productID)
    }

    class var unlockedApp: Bool {
        return UserDefaults.standard.bool(forKey: Constants.Store.productID)
    }
    
    class var baseVersion: String? {
        UserDefaults.standard.string(forKey: Constants.Labels.baseVersion)
    }
    
    class func setBaseVersion(value: String) {
        UserDefaults.standard.set(value, forKey: Constants.Labels.baseVersion)
    }
}

