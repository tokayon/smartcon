//
//  Constants.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 5/8/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit
import AVFoundation

struct Constants {

    static let version = "Version 1.1"
    static let tokayonSite = "https://tokayon.com/index.php/mobile-development/applications/"
    static let appEmail = "tokayonapps@gmail.com"
    static let privacyUrl = "https://tokayon.com/index.php/mobile-development/applications/smartcon/privacy-policy-sc/"
    static let termsUrl = "https://tokayon.com/index.php/mobile-development/applications/smartcon/terms-conditions-sc/"
    static let emailBody = "Please describe your issue..."
    static let emailSubject = "SmartCon Feedback"
    static let entityName = "ConvertEntity"
    static let modelName = "Convert"
    static let height = "Height"
    static let shoesM = "Shoes ♂︎"
    static let shoesW = "Shoes ♀︎"
    static let nameLength = 25
    static let abbrLength = 5
    static let regularPadding: CGFloat = 7.0
    static let equationPadding: CGFloat = 35.0
    static let nameTitle = "Name: "
    static let aIsTitle = "a: "
    static let bIsTitle = "b: "
    static let aEqualTitle = "a ="
    static let bEqualTitle = "b ="
    static let clickSound: SystemSoundID = UInt32(1306)
    static let hepticSound: SystemSoundID = UInt32(1102)
    
    struct Store {
        static let productID = "com.tokayon.smartCon.unlockedProduct"
        static let purchaseError = "Cannot connect to iTunes Store"
    }
    
    struct Controllers {
        static let alert = "AlertController"
    }
    
    struct Segues {
        static let customize = "CustomizeSegue"
        static let add = "AddSegue"
        static let details = "DetailsSegue"
        static let convertDetails = "ConvertDetailsSegue"
        static let tips = "TipsSegue"
        static let about = "AboutSegue"
        static let legal = "LegalSegue"
    }
    
    struct Labels {
        static let settings = "Settings"
        static let customize = "Customize"
        static let customizeConverters = "Customize Converters"
        static let restoreDefaults = "Restore Defaults"
        static let unlockApp = "Unlock the App"
        static let restorePurchases = "Restore Purchases"
        static let restored = "Restored Successfully"
        static let databaseError = "Database error"
        static let cantSendEmail = "Mail client doesn't configured"
        static let purchaseFailed = "Purchase failed"
        static let baseVersion = "BaseVersion"
        static let legal = "Legal"
        static let about = "About"
        static let error = "Err"
        static let na = "N/A"
        static let caution = "CAUTION"
        static let cancel = "CANCEL"
        static let capCancel = "Cancel"
        static let ok = "OK"
        static let add = "ADD"
        static let capAdd = "Add"
        static let done = "Done"
        static let edit = "Edit"
        static let save = "SAVE"
        static let yes = "YES"
        static let remove = "Remove"
        static let sureRemove = "Are you sure you want to remove?"
        static let purchaseAlert = "To add your own custom converts you need to purchase the unlocked app"
        static let newConvert = "New Convert"
        static let editConvert = "Edit Convert"
        static let notAllowed = "This name isn't allowed."
        static let shorterName = "Use shorter name"
        static let shorterAbbr = "Use shorter abbreviation"
        static let correct = "Equation is correct!"
        static let tipsTitle = "Tips to create equation:"
        static let firstNumber = "1.\n"
        static let secondNumber = "2.\n"
        static let thirdNumber = "3."
        static let fourthNumber = "4."
        static let firstTip = "Equation shall consist of\ntwo variables: a and b"
        static let firstTipExample = "a = 2 x b"
        static let secondTip = "Use operators before or\nafter parentheses"
        static let secondTipExample = "a = 2 x (5 - b)"
        static let thirdTip = "to check if equation is correct"
        static let fourthTip = "to swap variables in equation"
        static let smartCon = "SmartCon"
        static let developer = "Developer"
        static let developerName = "Serge Sinkevych"
        static let support = "Support"
        static let otherApps = "Other apps"
        static let privacy = "Privacy Policy"
        static let terms = "Terms & Conditions"
    }
}
