//
//  EquationManager.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 5/8/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

class EquationManager: NSObject {
    var operators: [Operator] = []
    var coeffs: [Double] = []
}

// MARK: - Checks
extension EquationManager {
    
    func checkAndCreate(equation: String) -> BuildResult {
        let check = quickCheck(equation: equation)
        switch check {
        case .success:
            return createEquation(from: equation)
        case .failure(let error):
            let equation = Equation(raw: equation)
            return BuildResult(equation: equation, error: error)
        }
    }
    
    private func quickCheck(equation: String) -> CheckResult {
        // check brackets
        if equation.isEmpty {
            return .failure(.emptyEquation)
        }
        
        if equation.contains("("), !equation.contains(")") {
            return .failure(.wrongEquation)
        }
        // check a or b
        if !checkFull(equation: equation) {
            return .failure(.notFullEquation)
        }
        // check + at the end
        if let last = equation.last, last.string.isSign {
            return .failure(.wrongEquation)
        }
        return .success
    }
    
    func check(input: String) -> String? {
        let dots = input.filter{ $0.string.isDot }.count
        guard dots < 2 else { return nil }
        
        if input.count == 1, input.isSign {
            return nil
        }
        
        let punctuation = CharacterSet(charactersIn: ".,")
        let digits = CharacterSet.decimalDigits
        let allowedCharSet = digits.union(punctuation)
        let inputCharSet = CharacterSet(charactersIn: input)
        
        if !(allowedCharSet.isSuperset(of: inputCharSet)) {
            return nil
        }
        
        return input
    }
      
    func check(value: String, after input: String, reverse: Bool) -> CheckResult {
        // check: a = a
        if !reverse, value.isA {
            return .failure(.doubleOperand)
        }
        // check: b = b
        if reverse, value.isB {
            return .failure(.doubleOperand)
        }
        // check: a = b b
        if !reverse, input.contains("b"), value.isB {
            return .failure(.doubleOperand)
        }
        // check: b = a a
        if reverse, input.contains("a"), value.isA {
            return .failure(.doubleOperand)
        }
        // check: = +
        if input.isEmpty, value.isSign {
            return .failure(.empty)
        }
        // check: ((
        if input.contains("("), value.isLBracket {
            return .failure(.empty)
        }
        // check: ))
        if input.contains(")"), value.isRBracket {
            return .failure(.empty)
        }
        // check: )
        if input.isEmpty, value.isRBracket {
            return .failure(.empty)
        }
        // check: no ) if no (
        if !input.contains("("), value.isRBracket {
            return .failure(.empty)
        }

        if let last = input.last {
            // check: +)
            if last.string.isSign, value.isRBracket {
                return .failure(.empty)
            }
            // check: (+
            if last.string.isLBracket, value.isSign {
                return .failure(.empty)
            }
            // check: ()
            if last.string.isLBracket, value.isRBracket {
                return .failure(.empty)
            }
            // check: ++
            if last.string.isSign, value.isSign {
                return .failure(.wrongOperatorOrder)
            }
            // check: 4( or a( or b(
            if !last.string.isSign, value.isLBracket {
                return .failure(.wrongOperandsOrder)
            }
            // check: )4 or )a or )b
            if last.string.isRBracket, !value.isSign {
                return .failure(.wrongOperandsOrder)
            }
            // check: 4a or 4b
            if !last.string.isSign, !last.string.isBracket, value.isAorB {
                return .failure(.wrongOperandsOrder)
            }
            // check: a4 or b4
            if last.string.isAorB, (!value.isSign && !value.isBracket) {
                return .failure(.wrongOperandsOrder)
            }
            // check: .+
            if last.string.isDot, value.isSign {
                return .failure(.empty)
            }
            // check: .) or .(
            if last.string.isDot, value.isBracket {
                return .failure(.empty)
            }
            // check: ...
            if last.string.isDot, value.isDot {
                return .failure(.empty)
            }
            // check: 1.3.4
            if value.isDot {
                for char in input.reversed() {
                    if char.string.isSign {
                        break
                    }
                    if char.string.isDot {
                        return .failure(.empty)
                    }
                }
            }
        }
        return .success
    }
    
    class func convertA(equation: String?) -> String {
        guard let equation = equation else { return "" }
        var replaced = equation.replacingOccurrences(of: "t", with: "b")
        replaced = replaced.replacingOccurrences(of: "*", with: "x")
        replaced = replaced.replacingOccurrences(of: "/", with: "÷")
        return replaced
    }
    
    class func convertB(equation: String?) -> String {
        guard let equation = equation else { return "" }
        var replaced = equation.replacingOccurrences(of: "t", with: "a")
        replaced = replaced.replacingOccurrences(of: "*", with: "x")
        replaced = replaced.replacingOccurrences(of: "/", with: "÷")
        return replaced
    }
}

// MARK: - Creation
private extension EquationManager {
    
    // replacing x->*, :->/, a->t, b->t
    func prepare(equation: String) -> String {
        var replaced = equation.replacingOccurrences(of: "x", with: "*")
        replaced = replaced.replacingOccurrences(of: "÷", with: "/")
        replaced = replaced.replacingOccurrences(of: "a", with: "t")
        replaced = replaced.replacingOccurrences(of: "b", with: "t")
        return replaced
    }
    
    func checkFull(equation: String) -> Bool {
        let a = equation.contains("a")
        let b = equation.contains("b")
        return a || b
    }
    
    func createEquation(from raw: String) -> BuildResult {
        let reverse = raw.contains("b")
        do {
            if reverse {
                let equation = try createReversedEquation(from: raw)
                return BuildResult(equation: equation)
            } else {
                let equation = try createDirectEquation(from: raw)
                return BuildResult(equation: equation)
            }
        } catch EquationError.zeroDivision  {
            let equation = Equation(raw: raw)
            return BuildResult(equation: equation, error: .zeroDivision)
        } catch {
            let equation = Equation(raw: raw)
            return BuildResult(equation: equation, error: .wrongEquation)
        }
    }
    
    func createDirectEquation(from raw: String) throws -> Equation {
        let prepared = prepare(equation: raw)
        try extract(equation: prepared)
        let regular = try buildEquation(ops: operators, coeffs: coeffs)
        let revertedOps = operators.reversed().map({ $0.reversed })
        let revertedCoeffs: [Double] = coeffs.reversed()
        let reverted = try buildEquation(ops: revertedOps, coeffs: revertedCoeffs)
        return Equation(raw: raw, a: regular, b: reverted)
    }
    
    func createReversedEquation(from raw: String) throws -> Equation {
        let prepared = prepare(equation: raw)
        try extract(equation: prepared)
        let regular = try buildEquation(ops: operators, coeffs: coeffs)
        let revertedOps = operators.reversed().map({ $0.reversed })
        let revertedCoeffs: [Double] = coeffs.reversed()
        let reverted = try buildEquation(ops: revertedOps, coeffs: revertedCoeffs)
        return Equation(raw: raw, a: reverted, b: regular)
    }
}

// MARK: - Computation
extension EquationManager {
    
    func compute(input: String, for equation: String?) -> String? {
        guard let equation = equation else { return nil }
        guard let checked = check(input: input) else {
            return Constants.Labels.error
        }
        let comaReplaced = checked.replacingOccurrences(of: ",", with: ".")
        let added = addToDot(input: comaReplaced)
        let replaced = equation.replacingOccurrences(of: "t", with: added)
        return compute(equation: replaced)
    }
    
    func addToDot(input: String) -> String {
        var output = input
        for (index, char) in input.enumerated() {
            if char == "." {
                if input.count > index + 1 {
                    if input[index+1].isSign {
                        let next = String.Index(utf16Offset: index+1, in: output)
                        output.insert("0", at: next)
                        return addToDot(input: output)
                    }
                } else {
                    return output + "0"
                }
            }
        }
        return output
    }
    
    func compute(equation: String) -> String {
        let result = compute(input: equation)
        guard result != .infinity else { return Constants.Labels.error }
        return result.string.precision(2)
    }

    func computeInchesHeight(input: String) -> (feet: String, inches: String) {
        guard let value = Double(input) else {
            return (Constants.Labels.error, Constants.Labels.error)
        }
        let inches = value / 2.54
        let absInches = inches >= 0 ? inches : -inches
        let feet = Int(absInches / 12.0)
        let remainder = inches - (Double(feet) * 12.0)
        return ("\(feet)", "\(Int(remainder))")
    }
    
    func computeCmHeight(feet: String, inches: String) -> String {
        let feetValue = feet.doubled
        let inchValue = inches.doubled
        let totalInches = feetValue * 12 + inchValue
        let cmValue = totalInches * 2.54
        return cmValue.string.precision(2)
    }
}

// MARK: - Private Configuration
private extension EquationManager {
    
    // compute expression, only numbers
    func compute(input: String) -> Double {
        let expression = NSExpression(format: input)
        guard let result = expression.expressionValue(with: nil, context: nil) as? Double else { return 0.0 }
        return result
    }
    
    // extract coeffs and operators to create an Equation
    func extract(equation: String) throws {
        if equation.contains("(") {
            try simplifyBrackets(equation: equation)
        } else {
            simplify(equation: equation)
        }
    }
    
    func buildEquation(ops: [Operator], coeffs: [Double]) throws -> String {
        var result = "t"
        for (index, op) in ops.enumerated() {
            let coeff = coeffs[index]
            let strCoeff = coeff < 0 ? "(\(coeff))" : "\(coeff)"
            switch op {
            case .plus:
                result = "(\(result)+\(strCoeff))"
            case .minus:
                result = "(\(result)-\(strCoeff))"
            case .multiply:
                result = "(\(result)*\(strCoeff))"
            case .divide:
                guard coeff != 0 else { throw EquationError.zeroDivision }
                result = "(\(result)/\(strCoeff))"
            case .inverse:
                result = "(\(strCoeff)/\(result))"
            default:
                continue
            }
        }
        return result
    }

    // extract equation in brackets
    // if brackets contains t:
    //      simplify brackets, replace brackets with t, simplify equation
    // if brackets without t:
    //      compute brackets, replace brackets with result, simplify equation
    func simplifyBrackets(equation: String) throws {
        let result = extractBrackets(equation: equation)
        var equation = equation
        switch result {
        case .success(let brackets):
            if brackets.content.contains("t") {
                simplify(equation: brackets.content)
                equation = equation.replacingCharacters(in: brackets.range, with: "t")
            } else {
                let computed = compute(input: brackets.content)
                equation = equation.replacingCharacters(in: brackets.range, with: "\(computed)")
            }
            simplify(equation: equation)
        case .failure(let error):
            throw error
        }
    }
    
    // extract equation in brackets and its range
    func extractBrackets(equation: String) -> BracketsResult {
        guard let openIndex = equation.indexOf(str: "("),
            let closeIndex = equation.indexOf(str: ")"),
            openIndex < closeIndex else { return .failure(.wrongEquation) }
        let output = String(equation[openIndex+1...closeIndex-1])
        let range = equation.index(at: openIndex)...equation.index(at: closeIndex)
        let result = BracketsContent(content: output, range: range)
        return .success(result)
    }
    
    // simplify all pre * and :, post * and :, and compute final result
    func simplify(equation: String) {
        let preInput = preEqualizer(input: equation)
        let postInput = postEqualizer(input: preInput)
        finalEqualizer(input: postInput)
    }
    
    // extract coeff and its length from string
    func extractCoeff(input: String) -> (coeff: String, length: Int) {
        var output = ""
        var length = 0
        for char in input {
            if Operator.convert(value: char) != .unknown, length > 0 {
                return (output, length)
            }
            output.append(char)
            length += 1
        }
        return (output, length)
    }
    
    // extract all * and / coeffs before tok
    func preEqualizer(input: String) -> String {
        // check if tok is in input
        guard let tindex = input.indexOf(str: "t") else { return input }
        // check if tok is not first
        guard tindex > 1 else { return input }
        let opIndex = String.Index(utf16Offset: tindex-1, in: input)
        let op = Operator.convert(value: input[opIndex])
        switch op {
        case .multiply, .divide:
            let toExtract = String(input.prefix(upTo: opIndex).reversed())
            let extracted = extractCoeff(input: toExtract)
            let reversedCoeff = String(extracted.coeff.reversed())
            coeffs.append(reversedCoeff.doubled)
            if op == .multiply { operators.append(.multiply) }
            if op == .divide { operators.append(.inverse) }
            let start = String.Index(utf16Offset: tindex - 1 - extracted.length, in: input)
            let end = String.Index(utf16Offset: tindex, in: input)
            let replaced = input.replacingCharacters(in: start...end, with: "t")
            return preEqualizer(input: replaced)
        case .minus:
            coeffs.append(-1.0)
            operators.append(.multiply)
            let replaced = input.replacingCharacters(in: opIndex...opIndex, with: "+")
            return replaced
        default:
            return input
        }
    }
    
    // extract all * and / coeffs after tok
    func postEqualizer(input: String) -> String {
        // check if tok is in input
        guard let tindex = input.indexOf(str: "t") else { return input }
        // check if tok is not last
        guard input.count > tindex + 2 else { return input }
        let opIndex = String.Index(utf16Offset: tindex + 1, in: input)
        let coeffIndex = String.Index(utf16Offset: tindex + 2, in: input)
        let op = Operator.convert(value: String(input[opIndex]))
        switch op {
        case .multiply, .divide:
            let toExtract = String(input.suffix(from: coeffIndex))
            let extracted = extractCoeff(input: toExtract)
            coeffs.append(extracted.coeff.doubled)
            if op == .multiply { operators.append(.multiply) }
            if op == .divide { operators.append(.divide) }
            let start = String.Index(utf16Offset: tindex, in: input)
            let end = String.Index(utf16Offset: tindex + 1 + extracted.length, in: input)
            let replaced = input.replacingCharacters(in: start...end, with: "t")
            return postEqualizer(input: replaced)
        default:
            return input
        }
    }
    
    // extract all final coeffs
    func finalEqualizer(input: String) {
        guard let tindex = input.indexOf(str: "t") else { return }
        guard input.count > 1 else { return }
        var result = 0.0
        // if b at the beginning, then extract just b
        if tindex == 0 {
            let extracted = String(input.dropFirst())
            if extracted.first == "+" {
                result = compute(input: String(extracted.dropFirst()))
            } else {
                result = compute(input: extracted)
            }
        } else {
            // check if there is coeff before b
            guard tindex > 1 else { return }
            let start = String.Index(utf16Offset: tindex - 1, in: input)
            let end = String.Index(utf16Offset: tindex, in: input)
            let extracted = input.replacingCharacters(in: start...end, with: "")
            result = compute(input: extracted)
        }
        coeffs.append(result)
        operators.append(.plus)
    }
}
