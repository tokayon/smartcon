//
//  TextfieldTag.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 11/28/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

enum TextfieldTag: Int {
    case left = 0
    case leftFirst = 1
    case leftSecond = 2
    case right = 3
    
    static func tag(_ tag: Int) -> TextfieldTag {
        switch tag {
        case 0:
            return .left
        case 1:
            return .leftFirst
        case 2:
            return .leftSecond
        case 3:
            return .right
        default:
            return .left
        }
    }
}
