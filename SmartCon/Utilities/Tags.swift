//
//  Tags.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/16/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import Foundation

enum Tags: Int {
    case nameField = 10
    case abbrField = 11
    case equationField = 12
    
    var tag: Int {
        return self.rawValue
    }
}
