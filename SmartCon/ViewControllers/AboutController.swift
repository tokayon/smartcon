//
//  AboutController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 3/24/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit
import MessageUI

class AboutController: BaseTableController {
    
    @IBOutlet weak var tableView: UITableView!
    
}

// MARK: - Setup
private extension AboutController {
    
    func configureCell(for indexPath: IndexPath) -> UITableViewCell {
        let cell: DetailsCell = tableView.dequeueReusableCell(for: indexPath)

        switch indexPath.row {
        case 0:
            cell.titleLabel.text = Constants.Labels.smartCon
            cell.detailsLabel.text = Constants.version
            cell.selectionStyle = .none
        case 1:
           cell.titleLabel.text = Constants.Labels.developer
           cell.detailsLabel.text = Constants.Labels.developerName
           cell.selectionStyle = .none
        case 2:
            cell.titleLabel.text = Constants.Labels.support
            cell.accessoryType = .disclosureIndicator
        case 3:
            cell.titleLabel.text = Constants.Labels.otherApps
            cell.accessoryType = .disclosureIndicator
        default:
            break
        }
        return cell
    }
    
    func sendEmailToSupport() {
      if MFMailComposeViewController.canSendMail() {
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self
        mail.setToRecipients([Constants.appEmail])
        mail.setSubject(Constants.emailSubject)
        
        mail.setMessageBody(Constants.emailBody, isHTML: true)

        present(mail, animated: true)
      } else {
        let popuper = PopupManager(delegate: self)
        popuper.cantSendEmail()
      }
    }
}

// MARK: - Mail
extension AboutController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK: - TableView
extension AboutController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0,1:
            return
        case 2:
            tableView.deselectRow(at: indexPath, animated: true)
            sendEmailToSupport()
        case 3:
            tableView.deselectRow(at: indexPath, animated: true)
            if let url = URL(string: Constants.tokayonSite) {
                UIApplication.shared.open(url)
            }
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return configureCell(for: indexPath)
    }
}
