//
//  ConvertController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 5/4/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit

class ConvertController: UIViewController {
    
    @IBOutlet weak var leftView: HalfView!
    @IBOutlet weak var rightView: HalfView!
    
    var convert: Convert!

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        cleanInfo()
    }
}

// MARK: - Setup
extension ConvertController {
    private func initialSettings() {
        setupViews()
    }
    
    private func cleanInfo() {
        leftView.cleanInfo()
        rightView.cleanInfo()
    }
    
    private func setupViews() {
        if convert.name == Constants.height {
            leftView?.oneField = false
        }
        leftView?.delegate = self
        leftView?.title.text = convert.a
        rightView?.delegate = self
        rightView?.title.text = convert.b
    }
}

extension ConvertController {
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let firstTouch = touches.first {
            let leftTouch = firstTouch.preciseLocation(in: leftView)
            let rightTouch = firstTouch.preciseLocation(in: rightView)
            if leftView.title.frame.contains(leftTouch) {
                cleanInfo()
                leftView.activateField()
            } else if rightView.title.frame.contains(rightTouch) {
                cleanInfo()
                rightView.activateField()
            } else {
                self.view.endEditing(true)
            }
        }
    }
}

extension ConvertController: HalfViewDelegate {
    func updated(tag: TextfieldTag, text: String) {
        let manager = EquationManager()
        switch tag {
        case .left:
            guard !text.isEmpty else {
                cleanInfo()
                return
            }
            
            switch convert.name {
            case Constants.shoesM, Constants.shoesW:
                guard let aData = convert.aData else { return }
                guard let doubled = Double(text) else { return }
                let converted = doubled.description
                guard let value = aData[converted] else {
                    rightView.field.text = Constants.Labels.na
                    return
                }
                rightView.field.text = value
            default:
                rightView.field.text = manager.compute(input: text, for: convert.equation?.b)
            }
            
        case .right:
            guard !text.isEmpty else {
                cleanInfo()
                return
            }
            
            switch convert.name {
            case Constants.height:
                let result = manager.computeInchesHeight(input: text)
                leftView.leftField.text = result.feet
                leftView.rightField.text = result.inches
            case Constants.shoesM, Constants.shoesW:
                guard let aData = convert.bData else { return }
                guard let doubled = Double(text) else { return }
                let converted = doubled.description
                guard let value = aData[converted] else {
                    leftView.field.text = Constants.Labels.na
                    return
                }
                leftView.field.text = value
            default:
                leftView.field.text = manager.compute(input: text, for: convert.equation?.a)
            }
        case .leftFirst:
            let inches = leftView.rightField.text ?? ""
            let result = manager.computeCmHeight(feet: text, inches: inches)
            rightView.field.text = result
        case .leftSecond:
            let feet = leftView.leftField.text ?? ""
            let result = manager.computeCmHeight(feet: feet, inches: text)
            rightView.field.text = result
        }
    }
}
