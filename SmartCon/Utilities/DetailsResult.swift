//
//  DetailsResult.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 1/15/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import Foundation

struct DetailsResult {
    var type: DetailsType
    var result: String?
}
