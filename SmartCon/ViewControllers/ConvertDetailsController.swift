//
//  ConvertDetailsController.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 2/26/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit

class ConvertDetailsController: BaseTableController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var removeButton: UIButton!
    
    var convert: Convert?

    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
        subscribeNotifications()
    }
    
    @IBAction func remove(_ sender: UIButton) {
        showRemovePopup()
    }
    
    
    deinit { unsubscribeNotifications() }
}

// MARK: - Notifications
extension ConvertDetailsController {
    
    func subscribeNotifications() {
        Notificator.add(self, sel: #selector(updateConvert), name: .update)
    }
    
    func unsubscribeNotifications() {
        Notificator.remove(self, name: .update)
    }
}

// MARK: - Setup
extension ConvertDetailsController {
    func initialSettings() {
        guard let convert = convert else { return }
        let edit = UIBarButtonItem(title: Constants.Labels.edit, style: .plain, target: self, action: #selector(editing))
        navigationItem.rightBarButtonItem = convert.permanent ? nil : edit
        navigationItem.title = convert.name
        removeButton.isHidden = convert.permanent
    }
    
    @objc func editing() {
        let navigator: NavController = instantiateViewController()
        let addController: AddController = navigator.instantiateChildController()
        addController.convert = convert
        self.present(navigator, animated: true, completion: nil)
    }
    
    func configureDetails(for cell: DetailsCell, at row: Int) -> DetailsCell {
        switch row {
        case 0:
            cell.titleLabel.text = Constants.aIsTitle
            cell.detailsLabel.text = convert?.a
        case 1:
            cell.titleLabel.text = Constants.bIsTitle
            cell.detailsLabel.text = convert?.b
        case 2:
            cell.titleLabel.text = Constants.aEqualTitle
            let converted = EquationManager.convertA(equation: convert?.equation?.a)
            cell.detailsLabel.attributedText = converted.attributedWhiteEquation
        case 3:
            cell.titleLabel.text = Constants.bEqualTitle
            let converted = EquationManager.convertB(equation: convert?.equation?.b)
            cell.detailsLabel.attributedText = converted.attributedWhiteEquation
        default:
            break
        }
        return cell
    }
    
    @objc func updateConvert() {
        guard let convert = convert else { return }
        guard let updated = DataManager.shared.fetchAndCreateConvert(name: convert.name) else { return }
        self.convert = updated
        self.tableView.reloadData()
    }
    
    func showRemovePopup() {
        guard let convert = convert else { return }
        let popuper = PopupManager(delegate: self)
        popuper.showRemovePopup {
            let result = DataManager.shared.delete(convert)
            switch result {
            case .success(_):
                self.navigationController?.popViewController(animated: true)
            default:
                break
            }
        }
    }
}

// MARK: - TableView
extension ConvertDetailsController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DetailsCell = tableView.dequeueReusableCell(for: indexPath)
        cell.selectionStyle = .none
        return configureDetails(for: cell, at: indexPath.row)
    }
}
