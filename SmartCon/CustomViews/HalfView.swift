//
//  HalfView.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 11/28/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit

class HalfView: UIView {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var field: UITextField!
    @IBOutlet weak var leftField: UITextField!
    @IBOutlet weak var rightField: UITextField!
    
    weak var delegate: HalfViewDelegate?
    
    var oneField = true {
        didSet {
            field?.isHidden = !oneField
            leftField?.isHidden = oneField
            rightField?.isHidden = oneField
        }
    }
    
    func activateField() {
        if oneField {
            field?.becomeFirstResponder()
        } else {
            leftField?.becomeFirstResponder()
        }
    }
    
    func cleanInfo() {
        field?.text = ""
        leftField?.text = ""
        rightField?.text = ""
    }
}

extension HalfView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let updated = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) {
            self.delegate?.updated(tag: TextfieldTag.tag(textField.tag), text: updated)
        }
        return true
    }
}

