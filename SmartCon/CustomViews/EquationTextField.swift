//
//  EquationTextField.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 12/28/19.
//  Copyright © 2019 Serge Sinkevych. All rights reserved.
//

import UIKit

class EquationTextField: TintTextField {
    
    @IBOutlet weak var equationTitle: UILabel!
    
    var titleIsHidden: Bool = true
    var reverse: Bool = false { didSet { setupEquationTitle() } }
    
    var padding: CGFloat {
        return titleIsHidden ? Constants.regularPadding : Constants.equationPadding
    }
    
    var title: String {
        return reverse ? Constants.bEqualTitle : Constants.aEqualTitle
    }

    var insets: UIEdgeInsets {
        let regular = Constants.regularPadding
        return UIEdgeInsets(top: regular, left: padding, bottom: regular, right: regular)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupEquationTitle()
    }

    // placeholder padding
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }

    // text padding
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: insets)
    }
    
    // disable paste
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}

extension EquationTextField {
    func setupEquationTitle() {
        guard let titleLabel = equationTitle else { return }
        titleLabel.isHidden = titleIsHidden
        titleLabel.text = title
    }
}
