//
//  PopupManager.swift
//  SmartCon
//
//  Created by Serge Sinkevych on 3/16/20.
//  Copyright © 2020 Serge Sinkevych. All rights reserved.
//

import UIKit

class PopupManager: NSObject {
    
    weak var delegate: UIViewController?
    
    init(delegate: UIViewController?) {
        self.delegate = delegate
    }
}

extension PopupManager {
    
    func showPopup(message: String) {
        guard let popup = instantiatePopupController() else { return }
        guard let delegate = delegate else { return }
        popup.message = message
        popup.secondTitle = Constants.Labels.ok
        popup.modalPresentationStyle = .overFullScreen
        popup.modalTransitionStyle = .crossDissolve
        delegate.present(popup, animated: true, completion: nil)
    }
    
    func showRestoredPopup() {
        showPopup(message: Constants.Labels.restored)
    }
    
    func showDatabaseErrorAlert() {
        showPopup(message: Constants.Labels.databaseError)
    }
    
    func cantSendEmail() {
        showPopup(message: Constants.Labels.cantSendEmail)
    }
    
    func showPurchaseFailed() {
        showPopup(message: Constants.Labels.purchaseFailed)
    }
    
    func showPurchasePopup(completion: @escaping () -> Void) {
        guard let popup = instantiatePopupController() else { return }
        guard let delegate = delegate else { return }
        var price = "."
        let unlockedProduct = StoreManager.shared.productsArray.first { $0.productIdentifier == Constants.Store.productID }
        if let product = unlockedProduct {
            let formatter = NumberFormatter()
            formatter.numberStyle = .currency
            formatter.locale = product.priceLocale
            let priceString = formatter.string(from: product.price) ?? "?"
            price = " for \(priceString)."
        }
        popup.message = Constants.Labels.purchaseAlert + price
        popup.firstTitle = Constants.Labels.ok
        popup.secondTitle = Constants.Labels.capCancel
        popup.firstAction = completion
        
        popup.modalPresentationStyle = .overFullScreen
        popup.modalTransitionStyle = .crossDissolve
        delegate.present(popup, animated: true, completion: nil)
    }
    
    func showRemovePopup(completion: @escaping () -> Void) {
        guard let popup = instantiatePopupController() else { return }
        guard let delegate = delegate else { return }
        popup.message = Constants.Labels.sureRemove
        popup.firstTitle = Constants.Labels.remove
        popup.secondTitle = Constants.Labels.capCancel
        popup.firstAction = completion
        
        popup.modalPresentationStyle = .overFullScreen
        popup.modalTransitionStyle = .crossDissolve
        delegate.present(popup, animated: true, completion: nil)
    }
}

private extension PopupManager {
    
    func instantiatePopupController() -> PopupController? {
        return self.delegate?.storyboard?.instantiateViewController(withIdentifier: "PopupController") as? PopupController
    }
}
